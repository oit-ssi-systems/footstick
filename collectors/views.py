from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Input, Software, Host, InputInstance
from datetime import timedelta
from django.template import loader
import sys
import json


def index(request):
    template = loader.get_template('collectors/index.html')
    return HttpResponse(template.render({}, request))


def hosts(request):
    template = loader.get_template('collectors/hosts.html')
    items = Host.objects.all()
    return HttpResponse(template.render({'items': items}, request))


def softwares(request):
    template = loader.get_template('collectors/softwares.html')
    items = Software.objects.all()
    return HttpResponse(template.render({'items': items}, request))


def software(request, name):
    item = Software.objects.get(name=name)
    template = loader.get_template('collectors/software.html')
    return HttpResponse(template.render({'item': item}, request))


def inputs(request):
    template = loader.get_template('collectors/inputs.html')
    items = Input.objects.all()
    return HttpResponse(template.render({'items': items}, request))


def input_instances(request):
    template = loader.get_template('collectors/input_instances.html')
    items = InputInstance.objects.all()
    return HttpResponse(template.render({'items': items}, request))


def input(request, software, name):
    item = Input.objects.get(software__name=software, name=name)
    template = loader.get_template('collectors/input.html')
    return HttpResponse(template.render({'item': item}, request))


def host(request, fqdn):
    item = Host.objects.get(fqdn=fqdn)
    template = loader.get_template('collectors/host.html')
    return HttpResponse(template.render({'item': item}, request))


@csrf_exempt
def ingest_telegraf(request):
    if request.method == 'POST':
        data = json.loads(request.body)

        software, _ = Software.objects.get_or_create(name='telegraf')
        for item in data:

            # Parse the host here
            host, created = Host.objects.get_or_create(
                fqdn=item['metadata']['hostname'])
            if created:
                sys.stderr.write("Created Host: %s\n" % host)

            # Parse the Input
            s_input, created = Input.objects.get_or_create(
                name=item['plugin_type'], software=software)
            if created:
                sys.stderr.write("Created Input: %s\n" % s_input)

            # Parse the Input Instance
            input_instance, created = InputInstance.objects.get_or_create(
                host=host,
                s_input=s_input,
                configuration=item['plugin_config'],
                interval=timedelta(seconds=item['metadata']['interval']))
            if created:
                sys.stderr.write("Created: %s\n" % input_instance)

            sys.stderr.write("%s\n" % item)

        return HttpResponse("Done")
