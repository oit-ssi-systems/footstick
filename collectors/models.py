from django.db import models
from taggit.managers import TaggableManager
from django.urls import reverse
import json


# Create your models here.
class Software(models.Model):
    name = models.CharField(max_length=200, unique=True)
    description = models.CharField(max_length=200, blank=True)
    tags = TaggableManager()

    def get_absolute_url(self):
        return reverse('software', args=[str(self.name)])

    def __str__(self):
        return "%s" % (self.name)


class Input(models.Model):
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=200, blank=True)
    software = models.ForeignKey(Software, on_delete=models.CASCADE)
    tags = TaggableManager()

    class Meta:
        unique_together = (('name', 'software'), )
        ordering = ['name']

    def get_absolute_url(self):
        return reverse('input', args=[str(self.software), str(self.name)])

    def __str__(self):
        return "%s" % (self.name)


class Host(models.Model):
    fqdn = models.CharField(max_length=200, unique=True)
    inputs = models.ManyToManyField(Input, blank=True)
    tags = TaggableManager()

    class Meta:
        ordering = ['fqdn']

    def get_absolute_url(self):
        return reverse('host', args=[str(self.fqdn)])

    def __str__(self):
        return "%s" % (self.fqdn)


class InputInstance(models.Model):
    host = models.ForeignKey(Host, on_delete=models.CASCADE)
    configuration = models.CharField(max_length=254)
    s_input = models.ForeignKey(Input, on_delete=models.CASCADE)
    interval = models.DurationField()
    tags = TaggableManager()

    def parse_configuration(self):
        return json.loads(self.configuration)

    class Meta:
        unique_together = (('host', 'configuration', 's_input'), )
        ordering = ['s_input__name']

    def __str__(self):
        return "%s - %s - %s" % (self.host, self.configuration,
                                 self.s_input.name)
