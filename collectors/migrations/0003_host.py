# Generated by Django 2.1.5 on 2019-04-17 14:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('collectors', '0002_auto_20190417_1402'),
    ]

    operations = [
        migrations.CreateModel(
            name='Host',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fqdn', models.CharField(max_length=200, unique=True)),
                ('inputs', models.ManyToManyField(blank=True, to='collectors.Input')),
            ],
        ),
    ]
