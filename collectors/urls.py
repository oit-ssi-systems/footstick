from django.urls import path
from . import views

urlpatterns = [
    path('', views.hosts, name='index'),
    path('hosts/', views.hosts, name='hosts'),
    path('host/<str:fqdn>/', views.host, name='host'),
    path('input/<str:software>/<str:name>/', views.input, name='input'),
    path('inputs/', views.inputs, name='inputs'),
    path('input_instances/', views.input_instances, name='input_instances'),
    path('softwares/', views.softwares, name='softwares'),
    path('software/<str:name>/', views.software, name='software'),
    path('ingest_telegraf/', views.ingest_telegraf, name='ingest_telegraf'),
]
