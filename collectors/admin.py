from django.contrib import admin
from .models import Software, Input, InputInstance, Host

# Register your models here.
admin.site.register(Software)
admin.site.register(Input)
admin.site.register(InputInstance)
admin.site.register(Host)
