def application_vars(request):
    return {
        'application_title':
        "Metrics Collectors",
        'application_name':
        "Metrics Collectors",
        'secondary_nav_links': ['<a href="/logout">Logout</a>'],
        'nav_links': [
            {
                'class': 'nav-link',
                'link': '<a href="/hosts" class="nav-link">Hosts</a>'
            },
            {
                'class': 'nav-link',
                'link': '<a href="/inputs" class="nav-link">Inputs</a>'
            },
            {
                'class': 'nav-link',
                'link':
                '<a href="/input_instances" class="nav-link">Instances</a>'
            },
            {
                'class': 'nav-link',
                'link': '<a href="/softwares" class="nav-link">Software</a>'
            },
        ]
    }
