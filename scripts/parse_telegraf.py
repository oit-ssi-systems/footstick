#!/usr/bin/env python3
import sys
import toml
from datetime import timedelta
import json


#  DS - Lovingly stolen from: https://bit.ly/2DgNFly
def convert_to_timedelta(time_val):
    """
    Given a *time_val* (string) such as '5d', returns a timedelta object
    representing the given value (e.g. timedelta(days=5)).  Accepts the
    following '<num><char>' formats:

    =========   ======= ===================
    Character   Meaning Example
    =========   ======= ===================
    s           Seconds '60s' -> 60 Seconds
    m           Minutes '5m'  -> 5 Minutes
    h           Hours   '24h' -> 24 Hours
    d           Days    '7d'  -> 7 Days
    =========   ======= ===================

    Examples::

        >>> convert_to_timedelta('7d')
        datetime.timedelta(7)
        >>> convert_to_timedelta('24h')
        datetime.timedelta(1)
        >>> convert_to_timedelta('60m')
        datetime.timedelta(0, 3600)
        >>> convert_to_timedelta('120s')
        datetime.timedelta(0, 120)
    """
    num = int(time_val[:-1])
    if time_val.endswith('s'):
        return timedelta(seconds=num)
    elif time_val.endswith('m'):
        return timedelta(minutes=num)
    elif time_val.endswith('h'):
        return timedelta(hours=num)
    elif time_val.endswith('d'):
        return timedelta(days=num)


def main():

    data = toml.loads(sys.stdin.read())

    global_interval_raw = data['agent'].get('interval')
    global_interval = convert_to_timedelta(global_interval_raw)
    global_tags_raw = data.get('global_tags', {})
    hostname = data['agent'].get('hostname')
    global_tags = []
    for k, v in global_tags_raw.items():
        global_tags.append("%s=%s" % (k, v))

    r_data = []
    for plugin_type, plugin_values in data['inputs'].items():
        for plugin_value in plugin_values:
            metadata = {}
            interval_raw = plugin_value.get('interval')
            if interval_raw:
                metadata['interval'] = convert_to_timedelta(
                    interval_raw).seconds
            else:
                metadata['interval'] = global_interval.seconds
            metadata['tags'] = global_tags
            metadata['hostname'] = hostname
            r_data.append({
                'plugin_type': plugin_type,
                'plugin_config': plugin_value,
                'metadata': metadata
            })
    print(json.dumps(r_data))
    return 0


if __name__ == "__main__":
    sys.exit(main())
